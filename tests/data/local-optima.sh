#! /bin/bash

. gen-helper.sh --directory=local-optima ${@:+"$@"}

for U in alice bob carol dave ellen francis georgina henry
do
    key "$U"
done

certify alice -a 120 -d 150 bob
certify bob -a 100 -d 50 carol
certify bob -a 50 -d 100 dave
certify bob -a 75 -d 200 francis
certify carol -a 100 -d 50 ellen
certify dave -a 50 -d 100 ellen
certify ellen -a 120 -d 100 francis
certify ellen -a 30 georgina
certify ellen henry

finish
