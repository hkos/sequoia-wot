#! /bin/bash

. gen-helper.sh --directory=best-via-root ${@:+"$@"}

key alice
key bob
key carol
key yellow
key zebra
key target

certify alice  -a 120 -d 10 bob
certify bob    -a 120 -d 10 carol
certify carol  -a 120 -d 10 target

certify alice  -a  50 -d 10 yellow
certify yellow -a  50 -d 10 zebra
certify zebra  -a  50 -d 10 target

finish
