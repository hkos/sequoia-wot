alice makes bob a trusted introducer for the "example.org" domain.

bob makes dave a trusted introducer for the "other.org" domain.

This means that A - B - D - E is invalid, because ed@example.org is
out of scope of the B - D delegation (i.e., it does not match
other.org).

```
                   alice@some.org
                          | 100/3/example.org
                   bob@example.org
        150/0   /                  \ 100/3/other.org
       carol@example.org         dave@other.org
                             100/0  / \ 100/0
                      ed@example.org   frank@other.org
```
