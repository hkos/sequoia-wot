#! /bin/bash

. gen-helper.sh --directory=roundabout ${@:+"$@"}

key alice
key bob
key carol
key dave
key elmar
key frank
key george
key henry
key isaac
key jenny

certify alice -a 60 -d 100 bob
certify alice -a 120 -d 6 carol
certify bob -a 120 -d 2 george
certify carol -a 120 -d 5 dave
certify dave -a 120 -d 4 elmar
certify elmar -a 120 -d 3 frank
certify frank -a 120 -d 2 bob
certify george -a 120 -d 1 henry
certify henry -a 120 -d 0 isaac
certify jenny -a 100 -d 5 elmar
certify jenny -a 100 -d 1 george

finish
