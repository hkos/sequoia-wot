#! /bin/bash

. gen-helper.sh --directory=isolated-root ${@:+"$@"}

t0=20200101
t1=20200201
t2=20200301
t3=20200401

key -t $t0 alice alice@other.org

revoke -t $t1 -u '<alice@example.org>' alice retired

finish
