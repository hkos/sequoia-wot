A straightforward certification chain.  Note: when A is the root, she
can authenticate D, but not E due to depth constraints.

```text
           o A
           |  2/100
           v
           o B
           |  1/100
           v
           o C                 o Frank
           |  1/100
           v
           o D
           |  1/100
           v
           o E
```
