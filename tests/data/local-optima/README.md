The best path from A to F is: A - B - C - E - F (amount: 100).  Back
propagation will choose: A - B - F (amount: 75), because it is
shorter.  Make sure we don't choose A - B - D - E - F.

For F, A - B - C - E is optimal (amount: 100).  Back propagation will
choose it, because at B, C - E and D - E have the same depth, but C -
E has a larger trust amount.

For G, A - B - C - E - G and A - B - D - E - G are equally good.  But,
we will select the latter, because when we have a choice (at E), we
prefer more residual depth.

For H, A - B - C - E - H is better.

Notation: amount/depth

```text
             A
             | 120/150
             v
             B -------------,
  100/50  /  |              |
         v   v 50/100       |
         C   D              |  75/200
  100/50  \  | 50/100       |
          _\|v              |
             o E --------   v
           /   \         `->F
    120/0 /     \ 30/0   120/100
         v       v
         H       G
```
