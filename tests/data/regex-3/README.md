Here we have multiple regular expressions on a single delegation.


```
                           alice@some.org
                              | 100/3/example.org|other.org
                    _     bob@example.org                  _
          100/0   /           | 100/3/their.org|other.org    \
    carol@example.org  _   dave@other.org   _                 henry@their.org
               100/0  /       | 100/0        \ 100/0
        ed@example.org     frank@other.org     george@their.org
```
