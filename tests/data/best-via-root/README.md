When doing backwards propagation, we find paths from all nodes to the
target.  Since we don't stop when we reach a root, the returned path
should still be optimal.  Consider:

```text
A --- 120/10 ---> B --- 120/10 ---> C --- 120/10 ---> Target
 \                                                      /
  `--- 50/10 ---> Y --- 50/10 ---> Z --- 50/10 --------'
```

When the root is B, then the path that we find for A should be `A -> B
-> C -> Target`, not `A -> Y -> Z -> Target`.
