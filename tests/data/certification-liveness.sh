#! /bin/bash

. gen-helper.sh --directory=certification-liveness ${@:+"$@"}

t0=20200101
t1=20200201
t2=20200301
t3=20200401

set -x

key -t $t0 alice
key -t $t0 bob
key -t $t0 carol

certify -t $t1 alice -a 60 -d 2 bob
certify -t $t1 bob -a 60 -d 1 carol

certify -t $t2 alice -e 31d -a 120 -d 2 bob
certify -t $t2 bob -e 31d -a 120 -d 1 carol

finish
