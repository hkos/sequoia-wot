#! /bin/bash

. gen-helper.sh --directory=certification-revoked ${@:+"$@"}

t0=20200101
t1=20200201
t2=20200301
t3=20200401

key -t $t0 alice
key -t $t0 bob
key -t $t0 carol

certify -t $t1 alice -a 60 -d 1 bob
certify -t $t1 bob -a 120 -d 0 carol

revoke -r alice -t $t2 -u '<bob@example.org>' bob retired

certify -t $t3 alice -a 120 -d 1 bob

finish
