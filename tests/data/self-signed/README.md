B tsigns <C, c1> and we want to authenticate c1.  This should work
when B is a root as C is considered a trusted introducer.  But it
should not work when A is considered a root a C can't introduce c2.

```
           A
    1/100  |
           B
1/90    /
      c1 - C - c2
           |
       120 |
           D
```
