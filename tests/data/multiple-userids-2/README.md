This test is similar to multiple-userids-1, but it uses regular
expressions.  Specifically, Alice certifies two different User IDs for
Bob.  One of them with a depth of 1 and no regular expression, and the
other scoped to other.org, but with a higher trust amount and more
depth.

```
                alice
               /     \
       50/1/* /       \ 70/255/@other.org
             /         \
  bob@some.org - bob - bob@other.org
                /   \
        120/2  /     \  120
              /       \
            carol      frank@other.org
        120 /    \ 120
dave@other.org    ed
```
