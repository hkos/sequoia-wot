If a User ID is revoked, then that overrides any later positive
certification.

We need to test three cases:

  1. We are authenticating a root binding whose User ID was revoked in
     the past.

  2. There is a valid path with length > 0 to a binding whose User ID
     is revoked.

  3. There is a valid path to some binding.  The path uses a
     certification of a revoked User ID.

In first two cases, it should not be possible to authenticate the
binding.  In the latter case, the revocation of the User ID should not
invalidate the delegation.

To test this, we use the following network:

```
  A
  | 2/60 at t1; 2/90 at t3
  v
  B  <- B's User ID is revoked at t2
  | 1/60 at t1; 1/90 at t3
  v
  C
```

Using the above network, we can test all three scenarios.
