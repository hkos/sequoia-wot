If the most recent certification has a trust amount of 0, then that
edge should not be considered.

To test this, we use the following network:

```
  A
  | 1/120
  v
  B
  | 1/60 at t1; 1/0 at t2
  v
  C
```

At t1, there is a path from A to C.  At t2, there should be no path
(not even one with a trust amount of 0!).
