#! /bin/bash

. gen-helper.sh --directory=infinity-and-beyond ${@:+"$@"}

set -e

for i in $(seq 1 260)
do
    key u$i
    if test x$i != x1
    then
       let h=$i-1
       certify u$h -a 120 -d 255 u$i
    fi
done

finish
