#! /bin/bash

. gen-helper.sh --directory=zero-trust ${@:+"$@"}

set -e

t0=20200101
t1=20200201
t2=20200301

key -t $t0 alice
key -t $t0 bob
key -t $t0 carol

certify -t $t1 alice -a 120 -d 1 bob
certify -t $t1 bob -a 60 -d 1 carol

certify -t $t2 bob -a 0 -d 1 carol

finish
