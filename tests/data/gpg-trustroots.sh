#! /bin/bash

. gen-helper.sh --directory=gpg-trustroots ${@:+"$@"}

t0=20200101
t1=20200201
t2=20200301
t3=20200401

key -t $t0 root
key -t $t0 a1
key -t $t0 a2
key -t $t0 a3

key -t $t0 d

key -t $t0 target

certify -t $t1 root a1
certify -t $t1 root a2
certify -t $t1 root a3

certify -t $t1 a1 d
certify -t $t1 a2 d
certify -t $t1 a3 d

certify -t $t1 d target

finish
