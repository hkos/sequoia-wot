#! /bin/bash

. gen-helper.sh --directory=self-signed ${@:+"$@"}

key alice
key bob
key carol carol@other.org
key dave

certify -a 100 -d 1 alice bob
certify -a 90 -d 1 bob carol
certify -a 120 carol dave

finish
