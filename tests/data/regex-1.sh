#! /bin/bash

set -e

. gen-helper.sh --directory=regex-1 ${@:+"$@"}

key alice@some.org
key bob
key carol
key dave@other.org
key ed
key frank@other.org

certify alice -a 100 -d 3 --domain=example.org bob
certify bob -a 100 -d 1 carol
certify bob -a 100 -d 1 --domain=other.org dave@other.org
certify dave -a 100 -d 1 ed
certify dave -a 100 -d 1 frank@other.org

finish
