How gpg interprets ownertrust is a bit complicated.  For a certificate
that is marked as "fully trusted" or "partially trusted" to be
considered a trust root, it also has to be reachable from an
ultimately trusted trust root.  Further, it is permissible for that to
happen via fully trusted or marginally trusted trust roots.  Consider:


```
             root
0/120    /    |     \
        a1    a2    a3
0/120    \    |     /
              d
              |
            target
```

Clearly, d cannot be authenticated from the root.  But if a1, a2, and
a3 are partially trusted trust roots, then it can be.  This means that
sq-wot has to iterate when adding gpg trust roots.
