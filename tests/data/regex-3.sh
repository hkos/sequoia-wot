#! /bin/bash

set -e

. gen-helper.sh --directory=regex-3 ${@:+"$@"}

key alice@some.org
key bob
key carol
key dave@other.org
key ed
key frank@other.org
key george@their.org
key henry@their.org

certify alice -a 100 -d 3 --domain=example.org --domain=other.org bob
certify bob -a 100 -d 1 carol
certify bob -a 100 -d 1 --domain=their.org --domain=other.org dave@other.org
certify bob -a 100 -d 1 henry@their.org
certify dave -a 100 -d 1 ed
certify dave -a 100 -d 1 frank@other.org
certify dave -a 100 -d 1 george@their.org

finish
