#! /bin/bash

. gen-helper.sh --directory=cycle ${@:+"$@"}

for U in alice bob carol dave ed frank
do
    key "$U"
done

certify alice -a 120 -d 3 bob
certify bob -a 90 -d 255 carol
certify carol -a 60 -d 255 dave
certify dave -a 120 -d 255 bob
certify dave -a 30 -d 1 ed
certify ed -a 120 -d 0 frank

finish
