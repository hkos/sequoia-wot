#! /bin/bash

set -e
set -x

faketime -f '-7d' bash -c '
set -e

. gen-helper.sh --directory=multiple-certifications-1 ${@:+"$@"}

key alice
key bob
key carol
key dave

certify alice -a 120 -d 255 bob
certify bob -a 120 -d 2 carol
certify carol -a 120 dave
'

# Override the old alice -> bob certification with two certifications
# made at the same time.

# We can freeze clock at an absolute timestamp by using: "YYYY-MM-DD
# hh:mm:ss"
T=$(date "+%Y-%m-%d %H:%M:%S")
faketime -f "$T" bash -c '
set -e

. gen-helper.sh --directory=multiple-certifications-1 ${@:+"$@"}

certify alice -a 50 -d 2 bob
certify alice -a 70 -d 1 bob

finish
'
