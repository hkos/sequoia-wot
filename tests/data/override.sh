#! /bin/bash

. gen-helper.sh --directory=override ${@:+"$@"}

key -t 20230113 alice
key -t 20230113 bob
key -t 20230113 carol

# On the 14th Alice makes Bob a trusted introducer.
certify alice -t 20230114 -a 120 -d 2 bob
# And Bob certifies Carol.
certify bob -t 20230114 -a 120 -d 0 carol

# On the 16th, Alice changes her mind.
certify alice -t 20230116 -a 120 -d 0 bob

finish
