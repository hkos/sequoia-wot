alice
 | t0: 2/120, t1: 0/120
 v
bob
 |  120
 v
carol

At t0, alice makes Bob a trusted introducer.  At t1, she issues
another certification, but only certifies bob.  Make sure that before
t1, alice can authenticate carol, but after t1 she can't.
