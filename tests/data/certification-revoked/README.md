Alice may realize that she made a certification in error, e.g., she
realizes that she was tricked into certifying an incorrect identity by
Mallory.  Or, circumstances may change.  A person may leave an
organization, so the CA admin needs to invalidate the certification of
their organizational identity.

Consider the following timeline:

  t0: A, B, and C are created
  t1: A certifies B and B certifies C.

```
  A
  | 1/60
  B
  | 0/120
  C
```

  t2: A revokes their certification of B

      A should now no longer be able to authenticate B or C.

  t3: A recertifies B

```
  A
  | 1/120
  B
  | 0/120
  C
```

      A should be able to authenticate B and C.
