use sequoia_openpgp as openpgp;

use openpgp::Result;

use assert_cmd::Command;

#[macro_use]
#[path = "../src/log.rs"]
mod log;

#[allow(unused)]
#[path = "../src/gpg.rs"]
mod gpg;

const TRACE: bool = false;

#[test]
fn gpg_trust_roots() -> Result<()> {
    let gnupghome = tempfile::tempdir()?;
    let gnupghome: &std::path::Path = gnupghome.as_ref();
    std::env::set_var("GNUPGHOME", gnupghome);

    // Make sure the keyring is empty.
    assert_eq!(gpg::export().unwrap(), vec![]);

    // Import the test data.
    let certs = include_bytes!("data/gpg-trustroots.pgp");
    gpg::import(certs)?;

    let ownertrust = b"\
# List of assigned trustvalues, created Wed 02 Mar 2022 12:01:08 PM CET
# (Use \"gpg --import-ownertrust\" to restore them)
D8330354E99DB503729A68D4AAE7E9EC2129CEC3:6:
80666EDD21A008D467243E47444D4C0F515D269A:4:
A6D2F50B1C9544A717B7625395FD89DA7093B735:4:
AFDD8AECD999F5CDC7027B23EECC4F0EA03A5F35:4:
BB0333A98A05430FF6A784A706D474BF36A3D4F9:4:
";
    gpg::import_ownertrust(ownertrust)?;


    // Check that we can authenticate the target.
    let mut cmd = Command::cargo_bin("sq-wot")?;
    cmd.args(&[
        "--gpg",
        "authenticate",
        "--partial",
        "30A185EA9319FF1D0BCBDBFCF2CD31DCC3DCAA02",
        "<target@example.org>",
    ]);

    let assertion = cmd.assert().success();

    let stdout
        = String::from_utf8_lossy(&assertion.get_output().stdout);
    assert!(stdout.contains(
        "[✓] 30A185EA9319FF1D0BCBDBFCF2CD31DCC3DCAA02 <target@example.org>: partially authenticated (33%)"));

    Ok(())
}
