This is a checklist for doing releases.

  1. Start from origin/main, create a branch `staging`
  1. Bump the version in Cargo.toml to XXX.
  1. Bump the version in README.md to XXX.
  1. Update dependencies and run tests.
       - Use the exact Rust toolchain version of the current Sequoia
         MSRV (refer to `README.md`):  `rustup default 1.xx`
       - Update the dependencies and run the tests:
```
cargo update
cargo build --release --features sequoia-openpgp/crypto-nettle
cargo test --release --features sequoia-openpgp/crypto-nettle
cargo doc --no-deps --release --features sequoia-openpgp/crypto-nettle
```
       - If some dependency is updated and breaks due to our MSRV,
         find a good version of that dependency and select it using
         e.g. `cargo update -p backtrace --precise 3.46`.
  1. Make a commit with the message `Release XXX.`.
       - Push this to gitlab as `staging`, create a merge
         request, wait for CI.
  1. Make sure `cargo publish` works:
       - `mkdir /tmp/sequoia-wot-staging`
       - `cd /tmp/sequoia-wot-staging`
       - `git clone git@gitlab.com:sequoia-pgp/sequoia-wot.git`
       - `cd sequoia-wot`
       - `git checkout origin/staging`
       - `cargo publish --dry-run --features sequoia-openpgp/crypto-nettle`
  1. Wait until CI and `cargo publish --dry-run` are successful.  In
     case of errors, correct them, and go back to the step creating
     the release commit.
  1. Merge the merge request
  1. Run `cargo publish`
  1. Make a tag `vXXX` with the message `Release XXX.` signed
     with an offline key, which has been certified by our
     `openpgp-ca@sequoia-pgp.org` key.
  1. Push the signed tag `vXXX`.
