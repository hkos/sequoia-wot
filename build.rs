use std::env;
use std::fs;
use std::io::{self, Write};
use std::path::PathBuf;

use clap_complete::Shell;

fn main() {
    include_test_data().unwrap();
    build_man_pages().unwrap();
    build_shell_completions().unwrap();
}

/// Builds the index of the test data for use with the `::tests`
/// module.
fn include_test_data() -> io::Result<()> {
    let cwd = env::current_dir()?;
    let mut sink = fs::File::create(
        PathBuf::from(env::var_os("OUT_DIR").unwrap())
            .join("tests.index.rs.inc")).unwrap();

    writeln!(&mut sink, "{{")?;
    let mut dirs = vec![PathBuf::from("tests/data")];
    while let Some(dir) = dirs.pop() {
        println!("rerun-if-changed={}", dir.to_str().unwrap());
        for entry in fs::read_dir(dir).unwrap() {
            let entry = entry?;
            let path = entry.path();
            if path.is_file() {
                writeln!(
                    &mut sink, "    add!({:?}, {:?});",
                    path.components().skip(2)
                        .map(|c| c.as_os_str().to_str().expect("valid UTF-8"))
                        .collect::<Vec<_>>().join("/"),
                    cwd.join(path))?;
            } else if path.is_dir() {
                dirs.push(path.clone());
            }
        }
    }
    writeln!(&mut sink, "}}")?;

    Ok(())
}

// We include cli.rs, which depends on a few data structures from
// sequoia_openpgp.  To avoid adding sequoia_openpgp as a build
// dependency, we provide stubs for the functionality that cli.rs
// uses.
mod openpgp {
    pub type Result<R, E=anyhow::Error> = anyhow::Result<R, E>;

    pub mod packet {
        use super::*;

        #[derive(Clone, Debug)]
        pub struct UserID {}

        impl From<&[u8]> for UserID {
            fn from(_: &[u8]) -> Self {
                UserID {}
            }
        }

        impl std::str::FromStr for UserID {
            type Err = anyhow::Error;

            fn from_str(_: &str) -> Result<Self> {
                Ok(UserID {})
            }
        }
    }

    #[derive(Clone, Debug)]
    pub struct KeyHandle {
    }

    impl std::str::FromStr for KeyHandle {
        type Err = anyhow::Error;

        fn from_str(_: &str) -> Result<Self> {
            Ok(KeyHandle {})
        }
    }
}

#[allow(unused)]
mod cli {
    include!("src/cli.rs");
}

fn build_man_pages() -> io::Result<()> {
    // Man page support.
    let out_dir = std::path::PathBuf::from(
        std::env::var_os("OUT_DIR").ok_or(std::io::ErrorKind::NotFound)?);

    use clap::CommandFactory;

    let man = clap_mangen::Man::new(cli::Cli::command());
    let mut buffer: Vec<u8> = Default::default();
    man.render(&mut buffer)?;

    let filename = out_dir.join("sq-wot.1");
    println!("cargo:warning=writing man page to {}", filename.display());
    std::fs::write(filename, buffer)?;

    for sc in cli::Cli::command().get_subcommands() {
        let man = clap_mangen::Man::new(sc.clone());
        let mut buffer: Vec<u8> = Default::default();
        man.render(&mut buffer)?;

        let filename = out_dir.join(format!("sq-wot-{}.1", sc.get_name()));
        println!("cargo:warning=writing man page to {}", filename.display());
        std::fs::write(filename, buffer)?;
    }

    Ok(())
}

fn build_shell_completions() -> io::Result<()> {
    let out_dir = std::path::PathBuf::from(
        std::env::var_os("OUT_DIR").ok_or(std::io::ErrorKind::NotFound)?);

    fs::create_dir_all(&out_dir).unwrap();

    use clap::CommandFactory;

    let mut sq_wot = cli::Cli::command();
    for shell in &[Shell::Bash, Shell::Fish, Shell::Zsh, Shell::PowerShell,
                   Shell::Elvish] {
        let path = clap_complete::generate_to(
            *shell, &mut sq_wot, "sq-wot", &out_dir)
            .unwrap();
        println!("cargo:warning=completion file is generated: {:?}", path);
    };

    Ok(())
}
