use std::borrow::Borrow;
use std::borrow::Cow;
use std::collections::HashMap;
use std::collections::HashSet;
use std::sync::Mutex;
use std::time::SystemTime;

use sequoia_openpgp as openpgp;
use openpgp::cert::prelude::*;
use openpgp::cert::raw::RawCert;
use openpgp::Fingerprint;
use openpgp::KeyHandle;
use openpgp::KeyID;
use openpgp::packet::UserID;
use openpgp::policy::Policy;
use openpgp::Result;

use sequoia_cert_store as cert_store;
use cert_store::LazyCert;
use cert_store::store::Store as _;
use cert_store::store::StoreError;
use cert_store::store::UserIDQueryParams;

use crate::Depth;
use crate::CertificationSet;
use crate::CertSynopsis;
use crate::store::Backend;
use crate::store::Store;

const TRACE: bool = false;

// Reimport crate as wot so that the links in the doc comments work.
#[allow(unused)]
use crate as wot;

/// A wrapper type for objects implementing
/// `cert_store::store::Store`.
///
/// This data type is a wrapper type for objects that implement
/// [`cert_store::store::Store`].  It implements
/// [`wot::store::Backend`] in terms of the `cert_store::store::Store`
/// interface, and exposes the underlying object via `Deref` and
/// `DerefMut`.
///
/// This wrapper is useful for constructing a [`Network`] from an
/// object that implements [`cert_store::store::Store`], like
/// [`cert_store::CertStore`].
///
/// Note: because the reference time is fixed, and you usually want to
/// use the current time as the reference time, you do not want to
/// hold onto this object for a long time.  As such, in long-running
/// programs, you should create this object on demand.  Happily,
/// creating a `CertStore` is inexpensive.  On the other hand,
/// creating a new `CertStore` for each query means that the `redge`
/// cache cannot be used.  As such, it is best to use a single
/// `CertStore` for a single operation, like authenticating some User
/// IDs for one or more certificates.
///
/// This wrapper caches the results of calls to
/// [`wot::store::Backend::redges`], which are typically expensive.
/// It includes a multi-threaded implementation of
/// [`wot::store::Backend::precompute`], which precomputes
/// [`wot::store::Backend::redges`] for all known bindings.
///
/// Finally, it includes an implementation of [`wot::store::Store`] in
/// terms of the [`cert_store::store::Store`] interface, which is much
/// more efficient than the default implementation.
///
/// [`Network`]: crate::Network
///
/// # Examples
///
/// ```
/// use std::borrow::Cow;
///
/// use sequoia_openpgp as openpgp;
/// use openpgp::cert::CertBuilder;
/// use openpgp::Fingerprint;
/// # use openpgp::Result;
/// use openpgp::packet::UserID;
/// use openpgp::policy::StandardPolicy;
///
/// use sequoia_cert_store as cert_store;
/// use cert_store::Store;
/// use sequoia_cert_store::StoreUpdate;
///
/// use sequoia_wot as wot;
/// use wot::Network;
/// use wot::Query;
/// use wot::Roots;
///
/// const P: &StandardPolicy = &StandardPolicy::new();
///
/// # fn main() -> Result<()> {
/// let (alice, _) = CertBuilder::general_purpose(None, Some("<alice@example.org>)"))
///     .generate()?;
/// let (bob, _) = CertBuilder::general_purpose(None, Some("<bob@example.org>"))
///     .generate()?;
///
/// let mut cert_store = cert_store::CertStore::empty();
/// cert_store.update(Cow::Owned(alice.clone().into()))?;
/// cert_store.update(Cow::Owned(bob.clone().into()))?;
///
/// // Build a WoT network.
/// let trust_roots = Roots::from(&[
///     (alice.fingerprint().into(), wot::FULLY_TRUSTED),
/// ]);
/// let wot_data = wot::store::CertStore::from_store(&cert_store, P, None);
/// let network = Network::new(&wot_data)?;
/// let q = Query::new(&network, trust_roots.clone());
///
/// // Try and authenticate Bob.
/// let paths = q.authenticate(
///     UserID::from("<bob@example.org>"),
///     bob.fingerprint(),
///     wot::FULLY_TRUSTED);
/// // Alice, our sole trust root, did not certify Bob so this will fail.
/// assert_eq!(paths.amount(), 0);
///
/// // Since network only has a reference to the cert_store, we don't
/// // have to do anything special to get cert_store back.
/// assert_eq!(cert_store.fingerprints().count(), 2);
/// // And thanks to nll, we can immediately transfer ownership.
/// drop(cert_store);
/// # Ok(()) }
pub struct CertStore<'a: 'ra, 'ra, S>
    where S: cert_store::store::Store<'a>,
{
    store: S,

    // Certifications on a certificate.
    //
    // Example:
    //
    // ```
    // 0xA certifies <Carol, 0xC>.    \ CertificationSet
    // 0xA certifies <Caroline, 0xC>. /
    // 0xB certifies <Carol, 0xC>.    > CertificationSet
    // ```
    //
    // The entry for 0xC has two `CertificationSet`s: one for those
    // made by 0xA and one for those made by 0xB.
    //
    // This is a cache, which is derived from the certs, and is update
    // lazily.
    redge_cache: Mutex<HashMap<Fingerprint, Vec<CertificationSet>>>,

    // The policy.  This is needed to compute the
    // certification sets.
    policy: &'ra dyn Policy,

    // The reference time.  This is needed to compute the
    // certification sets.
    reference_time: SystemTime,

    _a: std::marker::PhantomData<&'a ()>,
}


impl<'a: 'ra, 'ra, S> CertStore<'a, 'ra, S>
    where S: cert_store::store::Store<'a>,
{
    /// Returns a new `CertStore` from a cert store.
    ///
    /// A wrapper for [`cert_store::store::Store`]s, and implements
    /// [`store::Backend`] and [`store::Store`] on it.
    ///
    /// [`store::Backend`]: crate::store::Backend
    /// [`store::Store`]: crate::store::Store
    pub fn from_store<T>(store: S, policy: &'ra dyn Policy, t: T)
        -> Self
    where T: Into<Option<SystemTime>>,
    {
        let t = t.into().unwrap_or_else(SystemTime::now);

        Self {
            store,
            redge_cache: Default::default(),
            policy,
            reference_time: t,
            _a: std::marker::PhantomData,
        }
    }

    /// Returns a reference to the underlying store.
    pub fn store(&self) -> &S {
        &self.store
    }

    /// Returns a mutable reference to the underlying store.
    pub fn store_mut(&mut self) -> &mut S {
        &mut self.store
    }

    /// Returns the store.
    pub fn into_store(self) -> S {
        self.store
    }

    /// Returns the configured policy.
    pub fn policy(&self) -> &'ra dyn Policy {
        self.policy
    }

    /// Returns the configured reference time.
    pub fn reference_time(&self) -> SystemTime {
        self.reference_time.clone()
    }
}

impl<'a: 'ra, 'ra, S> std::ops::Deref for CertStore<'a, 'ra, S>
    where S: cert_store::store::Store<'a>,
{
    type Target = S;

    fn deref(&self) -> &Self::Target {
        &self.store
    }
}

impl<'a: 'ra, 'ra, S> std::ops::DerefMut for CertStore<'a, 'ra, S>
    where S: cert_store::store::Store<'a>,
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.store
    }
}

impl<'a: 'ra, 'ra> CertStore<'a, 'ra, cert_store::store::Certs<'a>>
{
    /// Returns a new `CertStore` from a slice of bytes.
    ///
    /// The bytes are interpreted as an OpenPGP keyring.  The data
    /// may, but need not, be ASCII-armor encoded.
    pub fn from_bytes<T>(bytes: &'a [u8],
                         policy: &'ra dyn Policy, t: T)
        -> Result<Self>
    where T: Into<Option<SystemTime>>,
    {
        tracer!(TRACE, "CertStore::from_bytes");

        let store = cert_store::store::Certs::from_bytes(bytes)?;

        let t = t.into().unwrap_or_else(SystemTime::now);

        Ok(Self {
            store,
            redge_cache: Default::default(),
            policy,
            reference_time: t,
            _a: std::marker::PhantomData,
        })
    }

    /// Creates a `CertStore` from some `Cert`s.
    pub fn from_certs<T>(certs: impl IntoIterator<Item=Cert>,
                         policy: &'ra dyn Policy, t: T)
        -> Result<Self>
    where T: Into<Option<SystemTime>>,
    {
        tracer!(TRACE, "CertStore::from_certs");

        let store = cert_store::store::Certs::from_certs(certs)?;

        let t = t.into().unwrap_or_else(SystemTime::now);

        Ok(Self {
            store,
            redge_cache: Default::default(),
            policy,
            reference_time: t,
            _a: std::marker::PhantomData,
        })
    }

    /// Creates a `CertStore` from `&Cert`s.
    pub fn from_cert_refs<T>(certs: impl IntoIterator<Item=&'a Cert>,
                             policy: &'ra dyn Policy, t: T)
        -> Result<Self>
    where T: Into<Option<SystemTime>>,
    {
        tracer!(TRACE, "CertStore::from_cert_refs");

        let store = cert_store::store::Certs::from_certs(
            certs.into_iter().map(|c| {
                let c: &'a Cert = c.into();
                LazyCert::from(c)
            }))?;

        let t = t.into().unwrap_or_else(SystemTime::now);

        Ok(Self {
            store,
            redge_cache: Default::default(),
            policy,
            reference_time: t,
            _a: std::marker::PhantomData,
        })
    }

    /// Creates a `CertStore` from `RawCert`s.
    pub fn from_raw_certs<T>(certs: impl IntoIterator<Item=RawCert<'a>>,
                             policy: &'ra dyn Policy, t: T)
        -> Result<Self>
    where T: Into<Option<SystemTime>>
    {
        tracer!(TRACE, "CertStore::from_raw_certs");

        let store = cert_store::store::Certs::from_certs(certs)?;

        let t = t.into().unwrap_or_else(SystemTime::now);

        Ok(Self {
            store,
            redge_cache: Default::default(),
            policy,
            reference_time: t,
            _a: std::marker::PhantomData,
        })
    }
}

impl<'a: 'ra, 'ra, S> cert_store::Store<'a> for CertStore<'a, 'ra, S>
    where S: cert_store::store::Store<'a>,
{
    fn lookup_by_cert(&self, kh: &KeyHandle) -> Result<Vec<Cow<LazyCert<'a>>>> {
        self.store.lookup_by_cert(kh)
    }

    fn lookup_by_cert_fpr(&self, fingerprint: &Fingerprint)
        -> Result<Cow<LazyCert<'a>>>
    {
        self.store.lookup_by_cert_fpr(fingerprint)
    }

    fn lookup_by_key(&self, kh: &KeyHandle) -> Result<Vec<Cow<LazyCert<'a>>>> {
        self.store.lookup_by_key(kh)
    }

    fn select_userid(&self, query: &UserIDQueryParams, pattern: &str)
        -> Result<Vec<Cow<LazyCert<'a>>>>
    {
        self.store.select_userid(query, pattern)
    }

    fn lookup_by_userid(&self, userid: &UserID) -> Result<Vec<Cow<LazyCert<'a>>>> {
        self.store.lookup_by_userid(userid)
    }

    fn grep_userid(&self, pattern: &str) -> Result<Vec<Cow<LazyCert<'a>>>> {
        self.store.grep_userid(pattern)
    }

    fn lookup_by_email(&self, email: &str) -> Result<Vec<Cow<LazyCert<'a>>>> {
        self.store.lookup_by_email(email)
    }

    fn grep_email(&self, pattern: &str) -> Result<Vec<Cow<LazyCert<'a>>>> {
        self.store.grep_email(pattern)
    }

    fn lookup_by_email_domain(&self, domain: &str) -> Result<Vec<Cow<LazyCert<'a>>>> {
        self.store.lookup_by_email_domain(domain)
    }

    fn fingerprints<'b>(&'b self) -> Box<dyn Iterator<Item=Fingerprint> + 'b> {
        self.store.fingerprints()
    }

    fn certs<'b>(&'b self)
        -> Box<dyn Iterator<Item=Cow<'b, LazyCert<'a>>> + 'b>
        where 'a: 'b
    {
        self.store.certs()
    }

    fn prefetch_all(&mut self) {
        self.store.prefetch_all()
    }

    fn prefetch_some(&mut self, certs: Vec<KeyHandle>) {
        self.store.prefetch_some(certs)
    }
}

impl<'a: 'ra, 'ra, S> Backend<'a> for CertStore<'a, 'ra, S>
    where S: cert_store::store::Store<'a>,
{
    /// Prefills the cache.
    ///
    /// Prefilling the cache makes sense when you plan to examine most
    /// nodes and edges in the network.  It doesn't make sense if you
    /// are just authenticating a single or a few bindings.
    ///
    /// This function is multi-threaded.
    ///
    /// Errors are silently ignored and are propagated when the
    /// operation in question is executed directly.
    fn precompute(&mut self) {
        tracer!(TRACE, "CertStore::precompute");
        t!("prefetching");

        // self.store is not (yet) thread self.  Extract the data
        // and stuff it in something that is thread safe.

        // Figure out what certificates we need to work on.
        let all: HashSet<Fingerprint>
            = HashSet::from_iter(self.store.fingerprints());
        let done: HashSet<Fingerprint>
            = HashSet::from_iter(
                self.redge_cache.lock().unwrap().keys().cloned());
        let todo: Vec<&Fingerprint>
            = all.difference(&done).collect();

        // As noted above, we can't work with LazyCerts, because they
        // are not Send or Sync.  We convert any RawCerts to Certs in
        // a hopefully multithreaded manner using prefetch.  And then
        // we work with Certs, which are Send and Sync.
        self.prefetch_all();

        // Build a list of the certs and an index from KeyID to index
        // in certs.
        let mut certs: Vec<Cert> = Vec::new();
        let mut cert_index: Vec<(KeyID, usize)> = Vec::new();

        for cert in self.store.certs() {
            let fpr = cert.fingerprint();
            match cert.as_cert() {
                Ok(cert) => {
                    let i = certs.len();

                    // XXX: We only need keys that are certification
                    // capable.  But we can't figure that out without
                    // creating a ValidCert and we don't want to do
                    // that slightly expensive operation here, where
                    // we are not multi-threaded.
                    for ka in cert.keys() {
                        cert_index.push((ka.keyid(), i));
                    }
                    certs.push(cert);
                }
                Err(err) => {
                    t!("invalid data for {}: {}", fpr, err);
                }
            }
        }
        cert_index.sort_unstable();

        // redges uses a Store to lookup certificates.  Satisfy it.
        struct C<'a>(&'a Vec<Cert>, &'a Vec<(KeyID, usize)>);

        impl<'a> cert_store::store::Store<'a> for C<'a>
        {
            fn lookup_by_cert(&self, kh: &KeyHandle)
                -> Result<Vec<Cow<LazyCert<'a>>>>
            {
                let r = self.lookup_by_key(kh)?
                    .into_iter()
                    .filter(|cert| cert.key_handle().aliases(kh))
                    .collect::<Vec<_>>();
                Ok(r)
            }

            fn lookup_by_key(&self, kh: &KeyHandle)
                             -> Result<Vec<Cow<LazyCert<'a>>>>
            {
                let keyid = KeyID::from(kh.clone());
                let id = &(keyid, 0);

                // We may have multiple certificates with the same
                // KeyID.  If we search by KeyID, then we'll find one.
                // But, we want them all.  So search for (keyid, 0),
                // which will give us the first one.
                let mut i = match self.1.binary_search(id) {
                    Ok(i) => i,
                    Err(i) => i,
                };

                let mut results = Vec::new();
                while i < self.1.len() && self.1[i].0 == id.0 {
                    let cert = &self.0[self.1[i].1];
                    if let KeyHandle::Fingerprint(fpr) = kh {
                        if fpr == &cert.fingerprint() {
                            results.push(Cow::Owned(LazyCert::from(cert)));
                        }
                    } else {
                        results.push(Cow::Owned(LazyCert::from(cert)));
                    }
                    i += 1;
                }

                if results.is_empty() {
                    Err(StoreError::NotFound(kh.clone()).into())
                } else {
                    Ok(results)
                }
            }

            fn select_userid(&self, _query: &UserIDQueryParams, _pattern: &str)
                             -> Result<Vec<Cow<LazyCert<'a>>>>
            {
                // This is not needed by Backend::redges, so it is
                // just a stub.
                Ok(Vec::new())
            }

            fn fingerprints<'b>(&'b self)
                -> Box<dyn Iterator<Item=Fingerprint> + 'b>
            {
                Box::new(self.0.iter().map(|c| c.fingerprint()))
            }
        }

        impl<'a> Backend<'a> for C<'a>
        {
        }

        let backend = C(&certs, &cert_index);

        // We could sort `todo` before distributing the work.  That
        // would ensure that any certificates with a lot of work are
        // done first and all threads will finish at about the same
        // time.  In practice: it's not worth it.

        use crossbeam::thread;
        use crossbeam::channel::unbounded as channel;

        // Avoid an extra level of indentation.
        let result = thread::scope(|thread_scope| {
        // The threads.  We start them on demand.
        let threads = if todo.len() < 16 {
            // The keyring is small, limit the number of threads.
            2
        } else {
            // Use at least one and not more than we have cores.
            num_cpus::get().max(1)
        };

        // A communication channel for sending work to the workers.
        let (work_tx, work_rx) = channel();

        let mut threads_extant = Vec::new();

        for fpr in todo.into_iter() {
            if threads_extant.len() < threads {
                let tid = threads_extant.len();
                t!("Starting thread {} of {}",
                   tid, threads);

                let mut work = Some(Ok(fpr));

                // The thread's state.
                let work_rx = work_rx.clone();

                let backend = &backend;
                let policy = self.policy;
                let reference_time = self.reference_time.clone();

                threads_extant.push(thread_scope.spawn(move |_| {
                    let mut results: Vec<(Fingerprint, Vec<CertificationSet>)>
                        = Vec::new();

                    loop {
                        match work.take().unwrap_or_else(|| work_rx.recv()) {
                            Err(_) => break,
                            Ok(fpr) => {
                                t!("Thread {} dequeuing {}!", tid, fpr);

                                // Silently ignore errors.  This will
                                // be caught later when the caller
                                // looks this one up.

                                let cert = if let Ok(cert)
                                    = backend.lookup_by_cert_fpr(fpr)
                                {
                                    cert
                                } else {
                                    continue;
                                };

                                match cert.with_policy(policy, reference_time)
                                {
                                    Ok(vc) => {
                                        results.push((
                                            cert.fingerprint(),
                                            backend.redges(vc, 0.into())))
                                    }
                                    Err(err) => {
                                        t!("{} is not valid under \
                                            the current policy: {}",
                                           cert.fingerprint(), err);
                                        results.push((
                                            cert.fingerprint(), Vec::new()))
                                    }
                                }
                            }
                        }
                    }

                    t!("Thread {} exiting", tid);

                    results
                }));
            } else {
                work_tx.send(fpr).unwrap();
            }
        }

        // When the threads see this drop, they will exit.
        drop(work_tx);

        let redges = threads_extant.into_iter().flat_map(|t| {
            let redges: Vec<(Fingerprint, Vec<CertificationSet>)>
                = t.join().unwrap();
            redges
        });

        // Add the results to the cache.
        self.redge_cache.lock().unwrap().extend(redges.into_iter());
        }); // thread scope.

        // We're just caching results so we can ignore errors.
        if let Err(err) = result {
            t!("{:?}", err);
        }
    }
}

impl<'a: 'ra, 'ra, S> CertStore<'a, 'ra, S>
    where S: cert_store::store::Store<'a>,
{
    fn certifications_of_uncached<F>(&self, target: F)
        -> Result<Vec<CertificationSet>>
    where F: Borrow<Fingerprint>
    {
        let target = target.borrow();
        let cert = self.store.lookup_by_cert_fpr(target)?;
        let redges = self.redges(
            cert.with_policy(self.policy, self.reference_time)?,
            0.into());

        Ok(redges)
    }

    fn to_synopsis(&self, cert: Cow<LazyCert>) -> Option<CertSynopsis> {
        cert
            .to_cert()
            .and_then(|c| {
                c.with_policy(self.policy, self.reference_time)
            })
            .map(Into::into)
            .ok()
    }
}

impl<'a: 'ra, 'ra, S> Store for CertStore<'a, 'ra, S>
    where S: cert_store::store::Store<'a>,
{
    fn reference_time(&self) -> SystemTime {
        self.reference_time
    }

    fn iter_fingerprints<'b>(&'b self) -> Box<dyn Iterator<Item=Fingerprint> + 'b> {
        tracer!(TRACE, "CertStore::iter_fingerprints");
        t!("");
        self.store.fingerprints()
    }

    fn synopses<'b>(&'b self) -> Box<dyn Iterator<Item=CertSynopsis> + 'b> {
        let certs = self.store
            .certs()
            .filter_map(|c| self.to_synopsis(c))
            .collect::<Vec<_>>();
        Box::new(certs.into_iter())
    }

    fn lookup_synopsis_by_fpr(&self, fingerprint: &Fingerprint)
        -> Result<CertSynopsis>
    {
        let cert = self.store.lookup_by_cert_fpr(fingerprint)?;
        self.to_synopsis(cert).ok_or_else(|| {
            StoreError::NotFound(KeyHandle::from(fingerprint.clone())).into()
        })
    }

    fn lookup_synopses(&self, kh: &KeyHandle) -> Result<Vec<CertSynopsis>> {
        tracer!(TRACE, "CertStore::lookup_synopses");

        t!("{}", kh);

        let certs: Vec<_> = self.store.lookup_by_cert(kh)?
            .into_iter()
            .filter_map(|c| self.to_synopsis(c))
            .collect();
        if certs.is_empty() {
            Err(StoreError::NotFound(kh.clone()).into())
        } else {
            Ok(certs)
        }
    }

    fn certifications_of(&self, target: &Fingerprint, _min_depth: Depth)
        -> Result<Vec<CertificationSet>>
    {
        tracer!(TRACE, "CertStore::certifications_of");

        t!("{}", target);

        let redge_cache = self.redge_cache.lock().unwrap();
        if let Some(redges) = redge_cache.get(target) {
            t!("Cache hit!");
            return Ok(redges.clone());
        }
        drop(redge_cache);

        t!("Cache miss!");

        let redges = self.certifications_of_uncached(target)?;

        self.redge_cache
            .lock().unwrap()
            .insert(target.clone(), redges.clone());

        Ok(redges)
    }

    fn lookup_synopses_by_userid(&self, userid: UserID) -> Vec<Fingerprint> {
        self.lookup_by_userid(&userid)
            .unwrap_or(Vec::new())
            .into_iter()
            .map(|c| c.fingerprint())
            .collect()
    }

    fn lookup_synopses_by_email(&self, email: &str) -> Vec<(Fingerprint, UserID)> {
        let email = if let Ok(email) = UserIDQueryParams::is_email(email) {
            email
        } else {
            return Vec::new();
        };

        self.lookup_by_email(&email)
            .unwrap_or(Vec::new())
            .into_iter()
            .flat_map(|cert| {
                cert.userids()
                    .filter_map(|userid| {
                        if let Ok(Some(e)) = userid.email() {
                            if e == email {
                                Some((cert.fingerprint(), userid.clone()))
                            } else {
                                None
                            }
                        } else {
                            None
                        }
                    })
                    .collect::<Vec<_>>()
                    .into_iter()
            })
            .collect()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    use std::borrow::Cow;

    use sequoia_openpgp as openpgp;
    use openpgp::cert::CertBuilder;
    use openpgp::Result;
    use openpgp::packet::UserID;
    use openpgp::policy::StandardPolicy;

    use sequoia_cert_store as cert_store;
    use cert_store::Store;
    use sequoia_cert_store::StoreUpdate;

    use crate::Network;
    use crate::Query;
    use crate::Roots;
    use crate::FULLY_TRUSTED;

    const P: &StandardPolicy = &StandardPolicy::new();

    // Check that that lifetimes allow us to use a CertStore as follow:
    #[test]
    fn cert_store_lifetimes() -> Result<()> {
        fn authenticate<'store: 'ra, 'ra>(
            // The core of our check.  Does the following compile:
            n: &Network<CertStore<'store, 'ra,
                                  &'ra cert_store::CertStore<'store>>>,
            trust_root: Fingerprint,
            target_fpr: Fingerprint,
            target_userid: UserID)
            -> usize
        {
            eprintln!("trust root: {}", trust_root);
            eprintln!("target: {}, {:?}", target_fpr, target_userid);

            let q = Query::new(&n, Roots::from(&[
                (trust_root, FULLY_TRUSTED),
            ]));

            let paths = q.authenticate(
                target_userid,
                target_fpr,
                FULLY_TRUSTED);

            eprintln!("paths: {:?}", paths);

            paths.amount()
        }

        let (alice, _) = CertBuilder::general_purpose(
            None, Some("<alice@example.org>"))
            .generate()?;
        let (bob, _) = CertBuilder::general_purpose(
            None, Some("<bob@example.org>"))
            .generate()?;

        let mut cert_store = cert_store::CertStore::empty();
        cert_store.update(Cow::Owned(alice.clone().into()))?;
        cert_store.update(Cow::Owned(bob.clone().into()))?;

        eprintln!("certificates:");
        for (i, cert) in cert_store.certs().enumerate() {
            eprintln!("  {}. {}, {}",
                      i,
                      cert.fingerprint(),
                      cert.userids().next().expect("have one"));
        }

        // Build a few WoT networks.
        for _ in 0..2 {
            let wot_data = CertStore::from_store(&cert_store, P, None);
            let network = Network::new(wot_data)?;

            // Try and authenticate Bob.
            let amount = authenticate(
                &network,
                alice.fingerprint(),
                bob.fingerprint(),
                UserID::from("<bob@example.org>"));

            // Alice, our sole trust root, did not certify Bob so this
            // will fail.
            assert_eq!(amount, 0);


            let amount = authenticate(
                &network,
                alice.fingerprint(),
                alice.fingerprint(),
                UserID::from("<alice@example.org>"));

            // Alice, our trust root, self signed the User ID, so this
            // should pass.
            assert_eq!(amount, FULLY_TRUSTED);
        }

        // Since network only has a reference to the cert_store, we don't
        // have to do anything special to get cert_store back.
        assert_eq!(cert_store.fingerprints().count(), 2);

        // And thanks to nll, we can recover ownership to the
        // underlying cert store.
        drop(cert_store);

        Ok(())
    }
}
