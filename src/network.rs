use std::fmt;
use std::time::SystemTime;
use std::ops::Deref;

use sequoia_openpgp as openpgp;

use openpgp::Result;
use openpgp::cert::prelude::*;
use openpgp::cert::raw::RawCert;
use openpgp::policy::Policy;

use sequoia_cert_store as cert_store;

use crate::CertSynopsis;
use crate::Certification;
use crate::store::CertStore;
use crate::store::Store;
use crate::store::SynopsisSlice;

pub(crate) mod filter;
mod root;
pub use root::Root;
mod roots;
pub use roots::Roots;
mod query;
pub use query::Query;
pub use query::QueryBuilder;
mod path;
pub use path::PathError;
pub use path::CertLints;
pub use path::CertificationLints;
pub use path::PathLints;
mod gossip;

use super::TRACE;

/// A certification network.
pub struct Network<S>
    where S: Store
{
    store: S,
}

impl<S> fmt::Debug for Network<S>
    where S: Store
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Network {{\n")?;
        write!(f, "  Reference time: {:?}\n", self.reference_time())?;
        write!(f, "  Nodes:\n")?;

        let mut certs: Vec<_> = self.synopses().map(|cert| {
            (
                cert.userids()
                    .map(|userid| {
                        String::from_utf8_lossy(userid.value())
                            .into_owned()
                    })
                    .collect::<Vec<String>>()
                    .join(", "),
                cert.fingerprint()
            )
        }).collect();
        certs.sort();

        for (userid, fpr) in certs {
            write!(f, "    {}: {}\n", fpr, userid)?;
        }

        write!(f, "  Edges:\n")?;

        let mut certifications = self.iter_fingerprints().filter_map(|fpr| {
            if let Ok(cs) = self.certifications_of(&fpr, 0.into()) {
                if cs.is_empty() {
                    None
                } else {
                    Some(cs)
                }
            } else {
                None
            }
        }).flatten().collect::<Vec<_>>();
        certifications.sort_by_key(|cs| {
            (cs.issuer().primary_userid().map(|u| u.userid().clone()),
             cs.issuer().fingerprint(),
             cs.target().fingerprint())
        });

        let mut last_issuer_fpr = None;
        for cs in certifications.into_iter() {
            let issuer = &cs.issuer();
            let issuer_fpr = issuer.fingerprint();
            if Some(&issuer_fpr) != last_issuer_fpr.as_ref() {
                write!(f, "    {} certifies:\n", issuer)?;
                last_issuer_fpr = Some(issuer_fpr);
            }

            for (userid, c) in cs.certifications() {
                for c in c.iter() {
                    write!(f, "      {}, {}: {}, {}, {}\n",
                           cs.target().fingerprint(),
                           userid.as_ref().map(|userid| {
                               String::from_utf8_lossy(userid.value()).into_owned()
                           }).unwrap_or_else(|| "<No User ID>".into()),
                           c.depth(), c.amount(),
                           if let Some(re_set) = c.regular_expressions() {
                               if re_set.matches_everything() {
                                   "*".into()
                               } else {
                                   format!("{:?}", re_set)
                               }
                           } else {
                               "<invalid RE>".into()
                           })?;
                }
            }
        }

        write!(f, "}}\n")?;

        Ok(())
    }
}

impl<S> Deref for Network<S>
    where S: Store
{
    type Target = S;

    fn deref(&self) -> &Self::Target {
        &self.store
    }
}

impl<S> Network<S>
    where S: Store
{
    /// Returns a Network.
    pub fn new(store: S)
        -> Result<Self>
    {
        tracer!(TRACE, "Network::new");

        Ok(Network {
            store: store,
        })
    }

    /// Returns a reference to the underlying store.
    pub fn backend(&self) -> &S {
        &self.store
    }
}

impl<'a: 'ra, 'ra> Network<CertStore<'a, 'ra, cert_store::store::Certs<'a>>> {
    /// Builds a web of trust network from a set of certificates.
    ///
    /// If a certificate is invalid according to the policy, the
    /// certificate is silently ignored.
    pub fn from_certs<I, C, T>(certs: I,
                               policy: &'ra dyn Policy, t: T)
        -> Result<Self>
    where T: Into<Option<SystemTime>>,
          I: IntoIterator<Item=C>,
          C: Into<Cert>,
    {
        tracer!(TRACE, "Network::from_certs");

        let t = t.into().unwrap_or_else(|| SystemTime::now());
        Network::new(CertStore::from_certs(
            certs.into_iter().map(|c| c.into()),
            policy, t)?)
    }

    /// Builds a web of trust network from a set of certificates.
    ///
    /// If a certificate is invalid according to the policy, the
    /// certificate is silently ignored.
    pub fn from_cert_refs<I, C, T>(certs: I,
                                   policy: &'ra dyn Policy, t: T)
        -> Result<Self>
    where T: Into<Option<SystemTime>>,
          I: IntoIterator<Item=C>,
          C: Into<&'a Cert>,
    {
        tracer!(TRACE, "Network::from_certs");

        let t = t.into().unwrap_or_else(|| SystemTime::now());
        Network::new(CertStore::from_cert_refs(
            certs.into_iter().map(|c| c.into()),
            policy, t)?)
    }

    /// Builds a web of trust network from a keyring.
    ///
    /// If a certificate is invalid according to the policy, the
    /// certificate is silently ignored.
    pub fn from_bytes<T>(certs: &'a [u8], policy: &'ra dyn Policy, t: T)
        -> Result<Self>
    where T: Into<Option<SystemTime>>,
    {
        tracer!(TRACE, "Network::from_bytes");

        let t = t.into().unwrap_or_else(|| SystemTime::now());
        Network::new(CertStore::from_bytes(certs, policy, t)?)
    }

    /// Builds a web of trust network from a set of raw certificates.
    ///
    /// If a certificate is invalid according to the policy, the
    /// certificate is silently ignored.
    pub fn from_raw_certs<T>(certs: impl Iterator<Item=RawCert<'a>>,
                             policy: &'a dyn Policy, t: T)
        -> Result<Self>
    where T: Into<Option<SystemTime>>,
    {
        tracer!(TRACE, "Network::from_raw_certs");

        let t = t.into().unwrap_or_else(|| SystemTime::now());
        Network::new(CertStore::from_raw_certs(certs, policy, t)?)
    }
}

impl<'a> Network<SynopsisSlice<'a>> {
    /// Builds a web of trust network from a set of certificates.
    ///
    /// If a certificate is invalid according to the policy, the
    /// certificate is silently ignored.
    pub fn from_synopses(certs: &'a [CertSynopsis],
                         certifications: &'a [Certification],
                         t: SystemTime)
        -> Result<Self>
    {
        Network::new(SynopsisSlice::new(certs, certifications, t)?)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    use openpgp::Fingerprint;
    use openpgp::packet::UserID;
    use openpgp::parse::Parse;
    use openpgp::policy::StandardPolicy;

    #[allow(unused)]
    #[test]
    fn third_party_certifications_of() -> Result<()> {
        let p = &StandardPolicy::new();

        let alice_fpr: Fingerprint =
            "2A2A4A23A7EEC119BC0B46642B3825DC02A05FEA"
           .parse().expect("valid fingerprint");
        let alice_uid
            = UserID::from("<alice@example.org>");

        let bob_fpr: Fingerprint =
            "03182611B91B1E7E20B848E83DFC151ABFAD85D5"
           .parse().expect("valid fingerprint");
        let bob_uid
            = UserID::from("<bob@other.org>");
        // Certified by: 2A2A4A23A7EEC119BC0B46642B3825DC02A05FEA
        let bob_some_org_uid
            = UserID::from("<bob@some.org>");
        // Certified by: 2A2A4A23A7EEC119BC0B46642B3825DC02A05FEA

        let carol_fpr: Fingerprint =
            "9CA36907B46FE7B6B9EE9601E78064C12B6D7902"
           .parse().expect("valid fingerprint");
        let carol_uid
            = UserID::from("<carol@example.org>");
        // Certified by: 03182611B91B1E7E20B848E83DFC151ABFAD85D5

        let dave_fpr: Fingerprint =
            "C1BC6794A6C6281B968A6A41ACE2055D610CEA03"
           .parse().expect("valid fingerprint");
        let dave_uid
            = UserID::from("<dave@other.org>");
        // Certified by: 9CA36907B46FE7B6B9EE9601E78064C12B6D7902


        let certs: Vec<Cert> = CertParser::from_bytes(
            &crate::testdata::data("multiple-userids-1.pgp"))?
            .map(|c| c.expect("Valid certificate"))
            .collect();
        let n = Network::from_cert_refs(certs.iter(), p, None)?;

        eprintln!("{:?}", n);

        // No one certified alice.
        assert!(
            n.third_party_certifications_of(&alice_fpr.clone())
                .is_empty());

        // Alice (and no one else) certified each of Bob's User IDs.
        let mut c = n.third_party_certifications_of(&bob_fpr);
        assert_eq!(c.len(), 2);
        c.sort_by_key(|c| (c.issuer().fingerprint(),
                           c.userid().map(Clone::clone)));
        assert_eq!(&c[0].issuer().fingerprint(), &alice_fpr);
        assert_eq!(c[0].userid(), Some(&bob_uid));
        assert_eq!(&c[1].issuer().fingerprint(), &alice_fpr);
        assert_eq!(c[1].userid(), Some(&bob_some_org_uid));

        Ok(())
    }

    #[allow(unused)]
    #[test]
    fn certified_userids_of() -> Result<()> {
        let p = &StandardPolicy::new();

        let alice_fpr: Fingerprint =
            "2A2A4A23A7EEC119BC0B46642B3825DC02A05FEA"
           .parse().expect("valid fingerprint");
        let alice_uid
            = UserID::from("<alice@example.org>");

        let bob_fpr: Fingerprint =
            "03182611B91B1E7E20B848E83DFC151ABFAD85D5"
           .parse().expect("valid fingerprint");
        let bob_uid
            = UserID::from("<bob@other.org>");
        // Certified by: 2A2A4A23A7EEC119BC0B46642B3825DC02A05FEA
        let bob_some_org_uid
            = UserID::from("<bob@some.org>");
        // Certified by: 2A2A4A23A7EEC119BC0B46642B3825DC02A05FEA

        let carol_fpr: Fingerprint =
            "9CA36907B46FE7B6B9EE9601E78064C12B6D7902"
           .parse().expect("valid fingerprint");
        let carol_uid
            = UserID::from("<carol@example.org>");
        // Certified by: 03182611B91B1E7E20B848E83DFC151ABFAD85D5

        let dave_fpr: Fingerprint =
            "C1BC6794A6C6281B968A6A41ACE2055D610CEA03"
           .parse().expect("valid fingerprint");
        let dave_uid
            = UserID::from("<dave@other.org>");
        // Certified by: 9CA36907B46FE7B6B9EE9601E78064C12B6D7902


        let certs: Vec<Cert> = CertParser::from_bytes(
            &crate::testdata::data("multiple-userids-1.pgp"))?
            .map(|c| c.expect("Valid certificate"))
            .collect();
        let n = Network::from_cert_refs(certs.iter(), p, None)?;

        eprintln!("{:?}", n);

        // There is the self signature.
        let mut c = n.certified_userids_of(&alice_fpr);
        assert_eq!(c.len(), 1);

        // Alice (and no one else) certified each of Bob's User IDs
        // for the two self signed User ID.
        let mut c = n.certified_userids_of(&bob_fpr);
        assert_eq!(c.len(), 2);
        c.sort_unstable();
        assert_eq!(&c[0], &bob_uid);
        assert_eq!(&c[1], &bob_some_org_uid);

        Ok(())
    }

    #[allow(unused)]
    #[test]
    fn certified_userids() -> Result<()> {
        let p = &StandardPolicy::new();

        let alice_fpr: Fingerprint =
            "2A2A4A23A7EEC119BC0B46642B3825DC02A05FEA"
           .parse().expect("valid fingerprint");
        let alice_uid
            = UserID::from("<alice@example.org>");

        let bob_fpr: Fingerprint =
            "03182611B91B1E7E20B848E83DFC151ABFAD85D5"
           .parse().expect("valid fingerprint");
        let bob_uid
            = UserID::from("<bob@other.org>");
        // Certified by: 2A2A4A23A7EEC119BC0B46642B3825DC02A05FEA
        let bob_some_org_uid
            = UserID::from("<bob@some.org>");
        // Certified by: 2A2A4A23A7EEC119BC0B46642B3825DC02A05FEA

        let carol_fpr: Fingerprint =
            "9CA36907B46FE7B6B9EE9601E78064C12B6D7902"
           .parse().expect("valid fingerprint");
        let carol_uid
            = UserID::from("<carol@example.org>");
        // Certified by: 03182611B91B1E7E20B848E83DFC151ABFAD85D5

        let dave_fpr: Fingerprint =
            "C1BC6794A6C6281B968A6A41ACE2055D610CEA03"
           .parse().expect("valid fingerprint");
        let dave_uid
            = UserID::from("<dave@other.org>");
        // Certified by: 9CA36907B46FE7B6B9EE9601E78064C12B6D7902


        let certs: Vec<Cert> = CertParser::from_bytes(
            &crate::testdata::data("multiple-userids-1.pgp"))?
            .map(|c| c.expect("Valid certificate"))
            .collect();
        let n = Network::from_cert_refs(certs.iter(), p, None)?;

        eprintln!("{:?}", n);

        // Alice is the root, but self signatures count, so there are
        // five certified User IDs in this network.
        let mut got = n.certified_userids();
        assert_eq!(got.len(), 5);

        got.sort_unstable();

        let mut expected = [
            (alice_fpr.clone(), alice_uid.clone()),
            (bob_fpr.clone(), bob_uid.clone()),
            (bob_fpr.clone(), bob_some_org_uid.clone()),
            (carol_fpr.clone(), carol_uid.clone()),
            (dave_fpr.clone(), dave_uid.clone()),
        ];
        expected.sort_unstable();

        assert_eq!(got, expected);

        Ok(())
    }
}
