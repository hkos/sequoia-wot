use sequoia_openpgp as openpgp;
use openpgp::Fingerprint;

use crate::FULLY_TRUSTED;

/// A trust root.
///
/// This describes a trust root.  A trust root is designated by its
/// fingerprint, and includes a degree of trust.  This is interpreted
/// the same as a certification's trust amount.  By default, trust
/// roots are fully trusted and thus have a trust amount of
/// [`FULLY_TRUSTED`].  However, it is possible to have trust roots
/// that are less strongly trusted.  These are created using
/// [`Root::new`].
#[derive(Debug, Clone)]
pub struct Root {
    fingerprint: Fingerprint,
    amount: usize,
}

impl Root {
    /// Returns a new `Root`.
    pub fn new(fingerprint: Fingerprint, amount: usize) -> Self {
        Self {
            fingerprint,
            amount,
        }
    }

    /// Returns the root's fingerprint.
    pub fn fingerprint(&self) -> &Fingerprint {
        &self.fingerprint
    }

    /// Returns the root's trust amount.
    pub fn amount(&self) -> usize {
        self.amount
    }

    /// Changes the root's trust amount.
    pub fn set_amount(&mut self, amount: usize) {
        self.amount = amount;
    }
}

impl From<Fingerprint> for Root {
    fn from(fingerprint: Fingerprint) -> Self {
        Root::new(fingerprint, FULLY_TRUSTED)
    }
}

impl From<&Fingerprint> for Root {
    fn from(fingerprint: &Fingerprint) -> Self {
        Root::from(fingerprint.clone())
    }
}

impl From<(Fingerprint, usize)> for Root {
    fn from(r: (Fingerprint, usize)) -> Self {
        Root::new(r.0, r.1)
    }
}

impl From<&(Fingerprint, usize)> for Root {
    fn from(r: &(Fingerprint, usize)) -> Self {
        Root::new(r.0.clone(), r.1)
    }
}

impl From<(&Fingerprint, usize)> for Root {
    fn from(r: (&Fingerprint, usize)) -> Self {
        Root::new(r.0.clone(), r.1)
    }
}
