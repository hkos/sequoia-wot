use std::collections::HashMap;

use sequoia_openpgp as openpgp;
use openpgp::Fingerprint;
use openpgp::KeyID;
use openpgp::packet::UserID;

use crate::Certification;
use crate::FULLY_TRUSTED;
use crate::Path;
use crate::Query;
use crate::network::Store;
use crate::network::filter::IdempotentCertificationFilter;
use crate::network::filter::TrustedIntroducerFilter;

use super::TRACE;
//const TRACE: bool = true;

impl<'a, S> Query<'a, S>
    where S: Store
{
    pub fn gossip(&self,
                  target_fpr: Fingerprint,
                  target_userid: UserID)
        -> HashMap<Fingerprint, (Path, usize)>
    {
        tracer!(TRACE, "Query::gossip");
        t!("{}, {:?}",
           KeyID::from(target_fpr.clone()),
           String::from_utf8_lossy(target_userid.value()));

        let mut results = if self.certification_network() {
            self.backward_propagate(
                target_fpr.clone(), target_userid.clone(),
                false,
                &TrustedIntroducerFilter::new())
        } else {
            self.backward_propagate(
                target_fpr.clone(), target_userid.clone(),
                false,
                &IdempotentCertificationFilter::new())
        };

        // Also check if it was self-certified.
        if let Ok(cert) = self.network().lookup_synopsis_by_fpr(&target_fpr) {
            if let Some(userid) = cert.userids().find(|u| {
                u.userid() == &target_userid
            })
            {
                let mut p = Path::new(cert.clone());
                let selfsig = Certification::new(
                    cert.clone(),
                    Some(target_userid),
                    cert.clone(),
                    userid.binding_signature_creation_time());
                if p.append(selfsig).is_ok() {
                    results.insert(target_fpr, (p, FULLY_TRUSTED));
                }
            }
        }

        results
    }
}
