use std::borrow::Borrow;
use std::collections::HashMap;

use sequoia_openpgp as openpgp;
use openpgp::Fingerprint;
use openpgp::packet::UserID;

use crate::FULLY_TRUSTED;
use crate::Network;
use crate::Path;
use crate::Paths;
use crate::Root;
use crate::Roots;
use crate::network::filter::CapCertificateFilter;
use crate::network::filter::ChainFilter;
use crate::network::filter::TrustedIntroducerFilter;
use crate::network::filter::SuppressCertificationFilter;
use crate::network::filter::SuppressIssuerFilter;
use crate::store::Store;
use crate::TRACE;

/// A builder for the parameters for a query on a network.
///
/// A `QueryBuilder` can be used to create a [`Query`] progressively.
pub struct QueryBuilder<'a, S>
    where S: Store
{
    // The underlying network.
    network: &'a Network<S>,

    // The trust roots.
    roots: Roots,

    // If this is a certification network (where all certificates are
    // considered tsigs with infiniate depth and no regular
    // expression), or a normal authentication network.
    certification_network: bool,
}

impl<'a, S> QueryBuilder<'a, S>
    where S: Store
{
    /// New.
    pub fn new(network: &'a Network<S>) -> Self
    {
        tracer!(TRACE, "QueryBuilder::new");

        Self {
            network,
            roots: Roots::empty(),
            certification_network: false,
        }
    }

    /// Sets the trust roots.
    pub fn roots<R>(&mut self, roots: R) -> &mut Self
        where R: Into<Roots>
    {
        let roots = roots.into();

        tracer!(TRACE, "QueryBuilder::roots");
        t!("Roots ({}): {}.",
           roots.iter().count(),
           roots.iter()
               .map(|r| format!("{} ({})", r.fingerprint(), r.amount()))
               .collect::<Vec<_>>()
               .join(", "));

        self.roots = roots;
        self
    }

    /// Configures the `Query` to treat the network as a certification
    /// network.
    ///
    /// By default, a `Query` works with an authentication network.
    /// In a certification network, all certifications are considered
    /// to be trusted signatures with an infinite trust depth, and no
    /// regular expressions.
    ///
    /// This is how most so-called *web-of-trust path-finder* or *pgp
    /// path-finder* algorithms work: they are interested in
    /// determining whether there is a chain of certifications from
    /// one certificate to another without regard as to whether a
    /// certifier considers the target to be a trusted introducer, or
    /// to have only verified the target's identity.
    pub fn certification_network(&mut self) -> &mut Self {
        self.certification_network = true;
        self
    }

    /// Configures the `Query` to treat the network as an
    /// authentication network.
    ///
    /// This is the default mode of operation where plain
    /// certifications are only considered certifications, and the
    /// target is not considered to be a trusted introducer.  An
    /// alternative mode of operation is a certification network.
    /// This can be configured using [`Query::certification_network`].
    /// See that method's documentation for more details.
    pub fn authentication_network(&mut self) -> &mut Self {
        self.certification_network = false;
        self
    }

    /// Returns a `Query`.
    pub fn build(&self) -> Query<'a, S> {
        Query {
            network: self.network,
            roots: self.roots.clone(),
            certification_network: self.certification_network,
        }
    }
}

/// The parameters for a query on a network.
///
/// A `Query` is a `Network` with a set of roots, and other
/// configuration parameters.
pub struct Query<'a, S>
    where S: Store
{
    // The underlying network.
    network: &'a Network<S>,

    // The trust roots.
    roots: Roots,

    // If this is a certification network (where all certifications
    // are considered tsigs with infinite trust depth, and no regular
    // expressions) or a normal authentication network.
    certification_network: bool,
}

impl<'a, S> Query<'a, S>
    where S: Store
{
    /// New.
    pub fn new<R>(network: &'a Network<S>, roots: R) -> Self
        where R: Into<Roots>
    {
        QueryBuilder::new(network)
            .roots(roots)
            .build()
    }

    /// Returns a reference to the network.
    pub fn network(&self) -> &'a Network<S>
    {
        &self.network
    }

    /// Returns a reference to the roots.
    pub fn roots(&self) -> &Roots
    {
        &self.roots
    }

    /// Returns whether the specified certificate is a root.
    pub fn is_root<F>(&self, fpr: F) -> bool
        where F: Borrow<Fingerprint>
    {
        self.roots.is_root(fpr.borrow())
    }

    /// Returns the specified root.
    pub fn root<F>(&self, fpr: F) -> Option<&Root>
        where F: Borrow<Fingerprint>
    {
        self.roots.get(fpr.borrow())
    }

    /// Returns whether the `Query` is working with a
    /// certification network.
    ///
    /// See [`Query::certification_network`] for
    /// details.
    pub fn certification_network(&self) -> bool {
        self.certification_network
    }

    /// Returns whether the `Query` is working with an
    /// authentication network.
    ///
    /// See [`Query::authentication_network`] for
    /// details.
    pub fn authentication_network(&self) -> bool {
        ! self.certification_network
    }

    /// Authenticates the specified binding.
    ///
    /// Enough independent paths are gotten to satisfy
    /// `target_trust_amount`.  A fully trusted authentication is 120.
    /// If you require that a binding be double authenticated, you can
    /// specify 240.
    pub fn authenticate<U, F>(&self, target_userid: U, target_fpr: F,
                              target_trust_amount: usize)
        -> Paths
    where U: Borrow<UserID>,
          F: Borrow<Fingerprint>,
    {
        tracer!(TRACE, "Query::authenticate");

        let target_userid = target_userid.borrow();
        let target_fpr = target_fpr.borrow();

        t!("Authenticating <{}, {}>",
           target_fpr, String::from_utf8_lossy(target_userid.value()));
        t!("Roots ({}):", self.roots.iter().count());
        for (i, r) in self.roots.iter().enumerate() {
            t!("  {}: {} ({})", i, r.fingerprint(), r.amount());
        }

        let mut paths = Paths::new();

        let mut filter = ChainFilter::new();
        if self.certification_network {
            // We're building a certification network: treat all
            // certifications like tsigs with infinite depth and no
            // regular expressions.
            filter.push(TrustedIntroducerFilter::new());
        } else {
            if self.roots.iter().any(|r| r.amount() != FULLY_TRUSTED) {
                let mut caps = CapCertificateFilter::new();
                for r in self.roots.iter() {
                    let amount = r.amount();
                    if amount != FULLY_TRUSTED  {
                        caps.cap(r.fingerprint().clone(), amount);
                    }
                }
                filter.push(caps);
            };
        }

        let mut progress = true;
        'next_path: while progress && paths.amount() < target_trust_amount {
            progress = false;

            for self_signed in [true, false] {
                let auth_paths: HashMap<Fingerprint, (Path, usize)>
                    = self.backward_propagate(
                        target_fpr.clone(), target_userid.clone(),
                        self_signed, &filter);

                // Note: the paths returned by backward_propagate may
                // overlap.  As such, we can only take one.  (Or we need
                // to subtract any overlap.  But that is fragile.)  Then
                // we subtract the path from the network and run
                // backward_propagate again, if necessary.
                if let Some((path, path_amount)) = self.roots.iter()
                    // Get the paths that start at the roots.
                    .filter_map(|r| {
                        auth_paths.get(r.fingerprint())
                    })
                    // Choose the one that: has the maximum amount of
                    // trust.  If there are multiple such paths, prefer
                    // the shorter one.
                    .max_by_key(|(path, path_amount)| {
                        (// We want the *most* amount of trust,
                            path_amount,
                            // but the *shortest* path.
                            -(path.len() as isize),
                            // Be predictable.  Break ties based on the
                            // fingerprint of the root.
                            path.root().fingerprint())
                    })
                {
                    let path = path.clone();

                    if path.len() == 1 {
                        // It's a root.
                        let mut suppress_filter
                            = SuppressIssuerFilter::new();
                        suppress_filter.suppress_issuer(
                            &path.root().fingerprint(), *path_amount);
                        filter.push(suppress_filter);
                    } else {
                        // Add the path to the filter to create a residual
                        // network without this path.
                        let mut suppress_filter
                            = SuppressCertificationFilter::new();
                        suppress_filter.suppress_path(&path, *path_amount);
                        filter.push(suppress_filter);
                    }

                    paths.push(path, *path_amount);
                    progress = true;
                    // Prefer paths where the target User ID is self
                    // signed as long as possible.
                    continue 'next_path;
                }
            }
        }

        paths
    }
}
